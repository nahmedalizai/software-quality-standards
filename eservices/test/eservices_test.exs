defmodule EservicesTest do
  use ExUnit.Case
  doctest Eservices

  test "User Login Pass" do
    assert true == Eservices.login("admin@email.com","admin000")
  end

  test "Username validation Fail with Length" do
    assert nil == Eservices.login("admin","admin000")
  end

  test "Username validation Fail Format" do
    assert nil == Eservices.login("admin00001234","admin000")
  end

  test "Password validation Fail" do
    assert nil == Eservices.login("admin@email.com","admin")
  end

  test "User Login Fail with Empty Inputs" do
    assert false == Eservices.login("","")
  end

  test "Two Step Verification Pass" do
    assert true == Eservices.twoStepVerification("0000")
  end

  test "Two Step Verification Fail" do
    assert false == Eservices.twoStepVerification("1111")
  end

  test "Request Service with correct selection" do
    assert :Success = Eservices.requestService(1,1)
    assert :Success = Eservices.requestService(2,4)
    assert :ok = Eservices.requestService(3,1)
    assert :ok = Eservices.requestService(4,1)
  end

  test "Request Service with wrong selection" do
    assert :Failure = Eservices.requestService(0,0)
  end

  test "Verify documents pass" do
    assert :verified = Eservices.verifyApplicantDocuments(["scannedId","scannedPhoto","applcationForm", "scannedPassport"])
  end

  test "Missing required document" do
    assert :notVerified = Eservices.verifyApplicantDocuments(["scannedPhoto","applcationForm"])
  end

  test "No documents submitted" do
    assert :notVerified = Eservices.verifyApplicantDocuments([])
  end

  test "Document submission" do
    assert :verified = Eservices.verifyApplicantDocuments(Eservices.verifiedDocumentSubmission())
  end

  test "make payment" do
    xs = [{222,100},{222,100},{222,100},{222,100},{222,100}]
    assert :ok = Eservices.makeAPayment(({55522,100}), xs)
  end

  test "check payment" do
    xs = [{222,100},{222,100},{222,100},{222,100},{123,111}]
    assert :paymentExists = Eservices.checkPayment(xs, 123)
  end

  test "check payment no made" do
    xs = [{222,100},{222,100},{222,100},{222,100}]
    assert :paymentNotExists = Eservices.checkPayment(xs, 123)
  end

  test " Reject Service - Already existing service" do
    assert :rejected = Eservices.rejectService({"John","19910101","EST"},["scannedId","scannedPhoto","applcationForm", "scannedPassport"])
  end

  test " Reject Service - Data incomplete name" do
    assert :rejected = Eservices.rejectService({"","19910101","EST"},["scannedId","scannedPhoto","applcationForm", "scannedPassport"])
  end

  test " Reject Service - Data incomplete date of birth" do
    assert :rejected = Eservices.rejectService({"John","","EST"},["scannedId","scannedPhoto","applcationForm", "scannedPassport"])
  end

  test " Reject Service - Data incomplete nationality" do
    assert :rejected = Eservices.rejectService({"John","19910101",""},["scannedId","scannedPhoto","applcationForm", "scannedPassport"])
  end

  test " Reject Service - Minimum required documents not provided" do
    assert :rejected = Eservices.rejectService({"Doe","19920101","RUS"},["scannedPhoto","applcationForm", "scannedPassport"])
  end

  test " Reject Service - Minimum required documents provided" do
    assert :notRejected = Eservices.rejectService({"Doe","19920101","RUS"},["scannedId","scannedPhoto","applcationForm", "scannedPassport"])
  end

  test " Notify User - Empty message" do
    assert :notSent = Eservices.notifyUser("", "+111111111111")
  end

  test " Notify User - Empty Mobile number" do
    assert :notSent = Eservices.notifyUser("dummy text sms message", "")
  end

  test " Notify User - Wrong Mobile number length" do
    assert :notSent = Eservices.notifyUser("dummy text sms message", "+1111111111")
  end

  test " Notify User - Sms sent" do
    assert :sent = Eservices.notifyUser("dummy text sms message", "+111111111111")
  end

end
