defmodule Eservices do

  def login(username,password) do
    if username != "" && password != "" do
      if validateUserName(username) do
          if validatePassword(password) do
            pin = "0000"
            twoStepVerification(pin)
          end
       end
    else 
      IO.puts "Empty username/password validation error"
      false 
    end
  end

  def validateUserName(username) do
    for <<c <- username>>, c != ?\s, into: "", do: <<c>>
    if String.length(username) < 10
      do 
        IO.puts "Username Min Length Validation error"
        false
    else
      if Regex.run(~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/, username) != nil
        do true
      else
        IO.puts "Username Format Validation error"
        false 
      end
    end
  end

  def validatePassword(password) do
    if String.length(password) != 8
      do 
        IO.puts "Password Validation error"
        false
    else
      IO.puts "Password Validated"
      true
    end
  end

  def twoStepVerification(pin) do
    if pin == "0000" do
      IO.puts "Login successful"
      true
    else
      IO.puts "Login unsuccessful"
      false
    end
  end

  def requestService(selection, noOfRequests) do 

    IO.puts "Services"
    IO.puts "1. Passport Services"
    IO.puts "2. ID Services"
    IO.puts "3. Driving License"
    IO.puts "4. Birth and Death Registration"
    #IO.gets("Please select your preferred service:\n", selection)
    if 0 < selection && selection < 5 do 
      case selection do 
          1 -> passportService()
          2 -> idService(noOfRequests)
          3 -> drivingLicense()
          4 -> bdRegistration()
      end
    else
      IO.puts "wrong selection"
      :Failure
    end
  end

  def sendInfo(reqData) do
    IO.puts "Your request has been logged and you can proceed to book an appointment"
    :Success

  end

  def passportService() do
    IO.puts "passportService"
    req = %Request{desc: "", data: ""}
    resp = sendInfo(req)
  end
  
  def idService(noOfRequests)
    when noOfRequests <= 1 do
      IO.puts "idService"
      resp = sendInfo(%Request{desc: "", data: ""})
  end

  def idService(noOfRequests) do
    IO.puts "idService"
    resp = sendInfo(%Request{desc: "", data: ""})
    idService(noOfRequests - 1)
  end
  
  def drivingLicense() do
    IO.puts "drivingLicense"
    :ok
  end
  
  def bdRegistration() do
    IO.puts "bdRegistration"
    :ok
  end

  def verifiedDocumentSubmission() do
    IO.puts "documentSubmission"
    ["scannedId","scannedPhoto","applcationForm"]
  end

  def verifyApplicantDocuments(submittedDocs) do
    IO.puts "verifyApplicantDocuments"
    minReqDocs = {"scannedId","scannedPhoto","applcationForm"}
    if(submittedDocs != nil) do
      doc1 = Enum.filter(submittedDocs, fn a -> (a == elem(minReqDocs,0)) end) 
      doc2 = Enum.filter(submittedDocs, fn a -> (a == elem(minReqDocs,1)) end)
      doc3 = Enum.filter(submittedDocs, fn a -> (a == elem(minReqDocs,2)) end)
      IO.puts Enum.count(doc1)
      IO.puts Enum.count(doc2)
      IO.puts Enum.count(doc3)
      if (Enum.count(doc1) == 1 && Enum.count(doc2) == 1 && Enum.count(doc3) == 1) do
        IO.puts "Documents Verified"
        :verified
      else
        IO.puts "At least one required document missing"
        :notVerified
      end
    end
  end

  def makeAPayment(newPaymentDetails,xsPayment) do
    [newPaymentDetails] ++ xsPayment
    IO.puts "makeAPayment"
    :ok
  end

  def checkPayment(xsPayment, userRef) do
    #IO.puts loopOverPayments(xsPayment, userRef) |> Enum.filter(fn a -> a != nil end)
    # for i <- xsPayment do
    #   if elem(i,0) == userRef do
    #     x ++ i
    #   end  
    # end
    ret = Enum.filter(xsPayment, fn ({ax,_}) -> ax == userRef end)
    if ret != [] do
      IO.puts "Payment exists"
      :paymentExists
    else
      IO.puts "Payment not exist"
      :paymentNotExists
    end
  end

  def rejectService(data,reqDocs) do
    if requestAlreadyExists(data) || isMissingData(data) do
      reason = "Rejection reason.."
      fillRejectionFormAndNotify(reason)
      IO.puts "rejected"
      :rejected
    else
      if verifyApplicantDocuments(reqDocs) == :verified do
        IO.puts "not rejected"
        :notRejected
      else
        reason = "At least one required document missing"
        fillRejectionFormAndNotify(reason)
        IO.puts "rejected"
        :rejected
      end
    end
  end

  def requestAlreadyExists(data) do
    if elem(data,0) == "John" && elem(data,1) == "19910101" && elem(data,2) == "EST" do
      IO.puts "request already exists"
      true
    else
      IO.puts "request does not exists"
      false
    end
  end

  def isMissingData(data) do
    if elem(data,0) == "" || elem(data,1) == "" || elem(data,2) == "" do
      IO.puts "some data is missing"
      true
    else
      IO.puts "no data is missing"
      false
    end
  end

  def fillRejectionFormAndNotify(reason) do
    IO.puts reason
    IO.puts "Applicant/user notified"
    notifyUser(reason,"+111111111111")
  end

  def notifyUser(message, mobileNo) do
    if message != "" && mobileNo != "" do
      if String.length(mobileNo) == 13 do
        IO.puts "call send sms service"
        :sent
      else 
        IO.puts "Wrong Mobile number"
        :notSent
      end
    else
      IO.puts "Message or mobile number is empty"
      :notSent  
    end
  end
  # def loopOverPayments(xsPayment, userRef) do
  #   x = []
  #   for i <- xsPayment do
  #     if elem(i,0) == userRef do
  #       x ++ i
  #     end 
  #   end
  # end
end
